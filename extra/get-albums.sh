#!/bin/sh
# File: get-albums.sh
# Location: extra/
# Author: bgstack15
# Startdate: 2022-07-07 13:50
# Title: Demo for getting albums
# Purpose: Download albums easily, but only keep the past so many days
# History:
# Usage:
#    adjust variables at top, and album names looped at the bottom.
# Reference:
# Improve:
#    switch to bash, and put the list of album names in the top part with the other variables?
# Dependencies:
# Documentation: see README.md for project

OUTDIR=/mnt/public/pictures
SCRIPT=./pp.py
USERNAME=admin
PWFILE=pwfile
URL=http://vm4:2342

get_album(){
   # Goal: get photos from this named album, and keep only ones from under $DAYS days ago.
   # call: get_album "${TOPDIR}" "${NAME}" "${DAYS}"
   _dir="${1}"
   _name="${2}"
   _days="${3}"
   when="$( date -d "-${_days} days" "+%F" )"
   test -d "${_dir}/${_name}" && find "${_dir}/${_name}" -mindepth 1 ! -type d -mtime "+${_days}" -delete
   "${SCRIPT}" --url "${URL}" --password "${PWFILE}" --username "${USERNAME}" -a "${_name}" --extra "&after=${when}" --directory "${_dir}"
}

for album in "Pretty Name 1" "Family Memories 2020-2025" ;
do
   get_album "${OUTDIR}" "${album}" "60"
done
