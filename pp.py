#!/usr/bin/env python3
# File: pp.py
# Location: photoprismpull/
# Author: bgstack15
# SPDX-License-Identifier: GPL-3.0
# Startdate: 2022-07-07 11:50
# Title: Photoprism Pull
# Project: photoprismpull
# Purpose: front-end for pplib
# History:
# Usage:
# Reference:
# Improve:
# Dependencies: pplib.py in this project
# Documentation: see README.md
import argparse, pplib, os

pp_version = "2022-07-08a"

parser = argparse.ArgumentParser(description="Interact with PhotoPrism API")
parser.add_argument("-p","--password","--pass", required=True, help="password or filename that contains password string.")
parser.add_argument("-u","--username","--user", help="username")
parser.add_argument("-r","--url", required=True, help="top url of PhotoPrism instance, e.g., http://vm4:2342")
action = parser.add_mutually_exclusive_group(required=True)
action.add_argument("-l","--list", action="store_true", help="List albums (titles and image count).")
action.add_argument("-a","--album", action="append", help="Download this album(s) by name. Can be called multiple times.")
parser.add_argument("-V","--version", action="version", version="%(prog)s "+pp_version)
parser.add_argument("-d","--dir","--directory", help="Top path to hold album directories. Default is {os.curdir}.")
parser.add_argument("-e","--extra","--extraparams","--extra-params", help="Additional query params to limit photos search within the album. See <https://github.com/photoprism/photoprism/blob/develop/internal/form/search_photos.go>")

args = parser.parse_args()
#print(args)
username = ""
password = ""
action = ""
directory = ""
extraparams = ""
if os.path.exists(args.password):
   with open(args.password,"r") as f:
      # Stripping this protects against an innocuous newline in the password file.
      password = f.read().rstrip('\n')
else:
    password = args.password
#print(f"using password {password}")
if args.list:
   action = "list"
else:
   action = "album"
url = ""
if "url" in args and args.url is not None:
   url = args.url
if "username" in args and args.username is not None:
   username = args.username
if "dir" in args and args.dir is not None:
   directory = args.dir
else:
   directory = os.curdir
if "extra" in args and args.extra is not None:
   extraparams = args.extra

#print(f"Taking action {action} for {url}")
if "list" == action:
   s = pplib.get_session(url, username, password)
   #print(f"Got session {s}")
   albums = pplib.get_albums(s)
   #print(albums)
   for a in albums:
      print(f"{a['PhotoCount']} {a['Title']}")
elif "album" == action:
   s = pplib.get_session(url, username, password)
   for a in args.album:
      _dir = os.path.join(directory,a)
      print(f"Fetching album \"{a}\" to \"{_dir}\"")
      pplib.download_album_to_directory(a,_dir,extraparams,s)
